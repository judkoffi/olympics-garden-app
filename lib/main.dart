import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:olympics_garden_app/views/home.dart';
import 'package:olympics_garden_app/utils/utils.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Boudchouuu',
      theme: ThemeData(
        primarySwatch: PRIMARY_COLOR,
        platform: TargetPlatform.iOS,
      ),
      debugShowCheckedModeBanner: false,
      home: HomeView(
        title: 'Mes réservations',
      ),
    );
  }
}

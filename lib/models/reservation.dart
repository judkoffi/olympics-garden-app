enum OlympicsSport {
  FOOTBALL,
  BASKETBALL,
}

class Reservation {
  int _id;
  String _title;
  String _date;
  String _hour;
  int _childNumber;
  String _userKey;

  Reservation(
    this._title,
    this._date,
    this._hour,
    this._childNumber,
    this._userKey,
  );

  Reservation.withId(
    this._id,
    this._title,
    this._date,
    this._hour,
    this._childNumber,
    this._userKey,
  );

  int get id => _id;

  String get title => _title;

  String get date => _date;

  String get hour => _hour;

  int get childNumber => _childNumber;

  String get userKey => _userKey;

  set title(String title) {
    _title = title;
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
    map['title'] = _title;
    map['hour'] = _hour;
    map['date'] = _date;
    map['userkey'] = _userKey;
    map['childNumber'] = _childNumber;
    return map;
  }

  // Extract a Note object from a Map object
  Reservation.fromMapObject(Map<String, dynamic> map) {
    this._title = map['title'];
    this._date = map['date'];
    this._hour = map['hour'];
    this._userKey = map['userkey'];
    this._childNumber = map['childNumber'];
  }
}

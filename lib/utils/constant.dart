const epreuve_coords = [
  {
    "latitude": "48.89425724937604",
    "longitude": "2.393233171491456",
    "name": "Zénith Paris"
  },
  {
    "latitude": "48.94343246612891",
    "longitude": "2.425972218122275",
    "name": "Village des médias"
  },
  {
    "latitude": "48.94243182280934",
    "longitude": "2.4176573669367",
    "name": "Le Bourget - Pavillon "
  },
  {
    "latitude": "48.93763970970383",
    "longitude": "2.418893565548635",
    "name": "Le Bourget - Pavillon "
  },
  {
    "latitude": "48.944602214056545",
    "longitude": "2.408848999300336",
    "name": "Stand de Tir"
  },
  {
    "latitude": "50.61192146512215",
    "longitude": "3.13051879339945",
    "name": "Stade Pierre-Mauroy"
  },
  {
    "latitude": "48.86143981248421",
    "longitude": "2.637255191412889",
    "tmp": "Point",
    "name": "Base nautique de Vaires-"
  },
  {
    "latitude": "45.76529499517082",
    "longitude": "4.982028996430796",
    "name": "Groupama Stadium"
  },
  {
    "latitude": "43.269826994682575",
    "longitude": "5.395887294485315",
    "name": "	Stade Velodrome 	"
  },
  {
    "latitude": "45.46073179626356",
    "longitude": "4.390116191849575",
    "name": "	Stade Geoffroy-Guichard 	"
  },
  {
    "latitude": "43.27340901401874",
    "longitude": "5.362309805284064",
    "name": "Marina	carte3d."
  },
  {
    "latitude": "43.705058999196666",
    "longitude": "	7.192584993825541",
    "name": "	Allianz Riviera	"
  },
  {
    "latitude": "48.890970126374356",
    "longitude": "	2.390915742637248",
    "name": "	Grande Halle de la "
  },
  {
    "latitude": "48.877734794754076",
    "longitude": "	2.282447217368629",
    "name": "	Hôtels CIO/IPC	"
  },
  {
    "latitude": "48.924529530483994",
    "longitude": "	2.360022060178832",
    "name": "	Stade de France	"
  },
  {
    "latitude": "48.92303503722434",
    "longitude": "	2.330769896188545",
    "name": "	Village olympique	"
  },
  {
    "latitude": "48.92623546126882",
    "longitude": "	2.354448436049054",
    "name": "	Centre Aquatique	"
  },
  {
    "latitude": "48.93971869658819",
    "longitude": "	2.386554475831765",
    "name": "	Piscine de Marville	"
  },
  {
    "latitude": "48.83857978324485",
    "longitude": "	2.378808258221038",
    "name": "	Accor Hotels Arena	"
  },
  {
    "latitude": "48.8566399276271",
    "longitude": "	2.296974215187934",
    "name": "	Champ de Mars	"
  },
  {
    "latitude": "48.85994342620015",
    "longitude": "	2.313120956312882",
    "name": "	Esplanade des Invalides	"
  },
  {
    "latitude": "48.85855287648155",
    "longitude": "	2.294012900887528",
    "name": "	Tour Eiffel	"
  },
  {
    "latitude": "48.87374799261201",
    "longitude": "	2.295058962086427",
    "name": "	Champs-Elysées	"
  },
  {
    "latitude": "48.86621100247427",
    "longitude": "	2.312015884782969",
    "name": "	Grand Palais	"
  },
  {
    "latitude": "48.83232287400628",
    "longitude": "	2.284458868683275",
    "name": "	Paris Expo - Hall "
  },
  {
    "latitude": "48.829822712277576",
    "longitude": "	2.289158872216094",
    "name": "	Paris Expo - Hall "
  },
  {
    "latitude": "48.835366679243265",
    "longitude": "	2.256496838154582",
    "name": "	Stade Pierre-de-Coubertin	"
  },
  {
    "latitude": "48.847146266783625",
    "longitude": "	2.249249517903197",
    "name": "	Roland-Garros	"
  },
  {
    "latitude": "48.895710282614345",
    "longitude": "	2.229658662928394",
    "name": "	U Arena	"
  },
  {
    "latitude": "48.84333317551031",
    "longitude": "	2.252739068540124",
    "name": "	Parc des Princes	"
  },
  {
    "latitude": "48.929343753318534",
    "longitude": "	2.247954003533052",
    "name": "	Stade Yves-du-Manoir	"
  },
  {
    "latitude": "48.78802902271954",
    "longitude": "	2.034890048326448",
    "name": "	Vélodrome National	"
  },
  {
    "latitude": "48.78814211959374",
    "longitude": "	1.966177220541377",
    "name": "	Colline d'Elancourt	"
  },
  {
    "latitude": "48.79198024283954",
    "longitude": "	2.015122171341755",
    "name": "	Piste BMX	"
  },
  {
    "latitude": "48.80488493566969",
    "longitude": "	2.119905349174121",
    "name": "	Château de Versailles"
  },
  {
    "latitude": "43.583565",
    "longitude": "	1.433666",
    "name": "	Stadium Municipal"
  },
  {
    "latitude": "44.89738099767311",
    "longitude": "	-0.5615529998791",
    "name": "Stade Matmut-Atlantique"
  },
  {
    "latitude": "47.256310621993265",
    "longitude": "-1",
    "name": "Point	Stade de la Beaujoire"
  },
  {
    "latitude": "48.75174663114575",
    "longitude": "	2.07613238573999",
    "name": "	Golf national"
  }
];

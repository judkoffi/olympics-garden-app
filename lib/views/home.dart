import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:olympics_garden_app/models/reservation.dart';
import 'package:olympics_garden_app/services/data_service.dart';
import 'package:olympics_garden_app/utils/utils.dart';
import 'package:olympics_garden_app/views/map.dart';
import 'package:olympics_garden_app/views/reservation_detail.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sqflite/sqflite.dart';

class HomeView extends StatefulWidget {
  HomeView({
    Key key,
    this.title,
  }) : super(key: key);
  final String title;

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final DataService database = DataService();
  final ScrollController _scrollController = ScrollController();
  final _refreshController = RefreshController(initialRefresh: false);
  var reservations = List<Reservation>();
  var count = 0;

  void _onRefresh() async {
    reservations = await database.getReservations();
    _refreshController.refreshCompleted();
    setState(() {});
  }

  void _onLoading() async {
    reservations = await database.getReservations();
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    _updateReservationList();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _updateReservationList() {
    final Future<Database> dbFuture = database.initializeDatabase();
    dbFuture.then((db) {
      database.getReservations().then((list) {
        setState(() {
          this.reservations = list;
          this.count = list.length;
        });
      });
    });
  }

  void _navigateToDetail(Reservation reservation, String title) async {
    var result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ReservationDetailView(
          reservation,
          title,
        ),
      ),
    );
    if (result) {
      _updateReservationList();
    }
  }

  getFirstLetter(String sport) {
    return sport.substring(0, 2);
  }

  void _delete(BuildContext context, Reservation reservation) async {
    print(reservation);
    var result = await database.deleteReservation(reservation.id);
    if (result != 0) {
      _showSnackBar(context, 'Reservation Deleted Successfully');
      _updateReservationList();
    }
  }

  void _edit(BuildContext context, Reservation reservation) async {
    _navigateToDetail(
      reservation,
      reservation.title,
    );
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  SmartRefresher _reservationListView() {
    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: false,
      controller: _refreshController,
      onRefresh: _onRefresh,
      onLoading: _onLoading,
      header: MaterialClassicHeader(
        backgroundColor: PRIMARY_COLOR,
        color: NEARLYDARKBLUE,
      ),
      child: ListView.builder(
        controller: _scrollController,
        itemCount: count,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int position) {
          return Card(
            color: Colors.white,
            elevation: 2.0,
            child: ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.amber,
                child: Text(
                  getFirstLetter(
                    this.reservations[position].title,
                  ),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              title: Text(
                this.reservations[position].title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                this.reservations[position].date.toString(),
              ),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  /* GestureDetector(
                    child: Icon(
                      Icons.mode_edit,
                      color: Colors.blueAccent,
                    ),
                    onTap: () {
                      _edit(
                        context,
                        reservations[position],
                      );
                    },
                  ),
                  GestureDetector(
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                    onTap: () {
                      _delete(
                        context,
                        reservations[position],
                      );
                    },
                  ),*/
                ],
              ),
              onTap: () {
                _navigateToDetail(
                  reservations[position],
                  reservations[position].title,
                );
              },
            ),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (reservations.isEmpty) {
      _updateReservationList();
    }

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/cooking.png',
                    ),
                    fit: BoxFit.contain,
                  ),
                ),
                padding: EdgeInsets.all(
                  5.0,
                ),
                child: new Text(''),
              ),
              ListTile(
                leading: Icon(
                  Icons.home,
                ),
                title: new Text('Home'),
                onTap: () {},
              ),
              Divider(
                color: NEARLYDARKBLUE,
                indent: 50,
              ),
              ListTile(
                leading: Icon(
                  Icons.zoom_out_map,
                ),
                title: new Text('Maps'),
                onTap: () async {
                  var result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MapView(
                        title: 'Maps',
                      ),
                    ),
                  );
                  if (result) {
                    _updateReservationList();
                  }
                },
              ),
              Divider(
                color: NEARLYDARKBLUE,
                indent: 50,
              ),
              ListTile(
                leading: Icon(Icons.shopping_basket),
                title: new Text('Shop'),
                onTap: () {},
              ),
              Divider(
                color: NEARLYDARKBLUE,
                indent: 50,
              ),
            ],
          ),
        ),
        body: _reservationListView(),
      ),
    );
  }
}

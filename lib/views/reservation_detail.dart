import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:olympics_garden_app/models/reservation.dart';
import 'package:olympics_garden_app/services/data_service.dart';

class ReservationDetailView extends StatefulWidget {
  final String appBarTitle;
  final Reservation reservation;

  ReservationDetailView(
    this.reservation,
    this.appBarTitle,
  );

  @override
  State<StatefulWidget> createState() {
    return ReservationDetailViewState(this.reservation, this.appBarTitle);
  }
}

class ReservationDetailViewState extends State<ReservationDetailView> {
  DataService database = DataService();

  String appBarTitle;
  Reservation reservation;

  var titleController = TextEditingController();
  var dateController = TextEditingController();
  var hourController = TextEditingController();
  var childNumberController = TextEditingController();
  var userKeyController = TextEditingController();

  ReservationDetailViewState(
    this.reservation,
    this.appBarTitle,
  );

  @override
  Widget build(BuildContext context) {
    var textStyle = Theme.of(context).textTheme.title;

    titleController.text = reservation.title;
    dateController.text =
        DateFormat.yMd().format(DateTime.parse(reservation.date)).toString();
    hourController.text =
        DateFormat.Hm().format(DateTime.parse(reservation.hour)).toString();
    childNumberController.text = reservation.childNumber.toString();
    userKeyController.text = reservation.userKey;

    return WillPopScope(
      onWillPop: () {
        _moveToLastScreen();
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(appBarTitle),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => _moveToLastScreen(),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.only(
            top: 15.0,
            left: 10.0,
            right: 10.0,
          ),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  top: 15.0,
                  bottom: 15.0,
                ),
                child: TextField(
                  controller: titleController,
                  style: textStyle,
                  enabled: false,
                  onChanged: (value) {
                    debugPrint('Something changed in Title Text Field');
                  },
                  decoration: InputDecoration(
                    labelText: 'Title',
                    labelStyle: textStyle,
                    enabled: false,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: TextField(
                  controller: dateController,
                  style: textStyle,
                  onChanged: (value) {
                    debugPrint('Something changed in Description Text Field');
                  },
                  enabled: false,
                  decoration: InputDecoration(
                    labelText: 'Date',
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: TextField(
                  controller: hourController,
                  style: textStyle,
                  onChanged: (value) {
                    debugPrint('Something changed in Description Text Field');
                  },
                  enabled: false,
                  decoration: InputDecoration(
                    labelText: 'Hour',
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: TextField(
                  controller: childNumberController,
                  style: textStyle,
                  onChanged: (value) {
                    debugPrint('Something changed in Description Text Field');
                  },
                  enabled: false,
                  decoration: InputDecoration(
                    labelText: 'Nombre d\'enfant',
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
              
              /*Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        color: Theme.of(context).primaryColorDark,
                        textColor: Theme.of(context).primaryColorLight,
                        child: Text(
                          'Save',
                          textScaleFactor: 1.5,
                        ),
                        onPressed: () {
                          setState(() {
                            debugPrint("Save button clicked");
                            _save();
                          });
                        },
                      ),
                    ),
                    Container(
                      width: 5.0,
                    ),
                    Expanded(
                      child: RaisedButton(
                        color: Theme.of(context).primaryColorDark,
                        textColor: Theme.of(context).primaryColorLight,
                        child: Text(
                          'Delete',
                          textScaleFactor: 1.5,
                        ),
                        onPressed: () {
                          setState(() {
                            debugPrint("Delete button clicked");
                            _delete();
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),*/
            ],
          ),
        ),
      ),
    );
  }

  void _moveToLastScreen() {
    Navigator.pop(context, true);
  }

  void _save() async {
    _moveToLastScreen();

    int result;
    if (reservation.id != null) {
      result = await database.updateReservation(reservation);
    } else {
      result = await database.insertReservation(reservation);
    }

    if (result != 0) {
      _showAlertDialog('Status', 'Reservation Saved Successfully');
    } else {
      _showAlertDialog('Status', 'Problem Saving Reservation');
    }
  }

  void _delete() async {
    _moveToLastScreen();

    if (reservation.id == null) {
      _showAlertDialog('Status', 'No Reservation was deleted');
      return;
    }

    var result = await database.deleteReservation(reservation.id);
    if (result != 0) {
      _showAlertDialog('Status', 'Reservation Deleted Successfully');
    } else {
      _showAlertDialog('Status', 'Error Occured while Deleting Reservation');
    }
  }

  void _showAlertDialog(String title, String message) {
    var alertDialog = AlertDialog(
      title: Text(title),
      content: Text(message),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }
}

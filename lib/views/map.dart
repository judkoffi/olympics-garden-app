import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:olympics_garden_app/utils/constant.dart';
import 'package:olympics_garden_app/utils/utils.dart';
import 'package:olympics_garden_app/views/custom_dialog.dart';

class MapView extends StatefulWidget {
  MapView({
    Key key,
    this.title,
  }) : super(key: key);
  final String title;

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  MapController _controller = MapController();

  List<Marker> markers = List();

  showPopUp(name, context) {
    showDialog(
      context: context,
      builder: (BuildContext context) => CustomDialog(
        title: name,
        description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      ),
    );
  }

  @override
  void initState() {
    markers = epreuve_coords
        .map(
          (m) => Marker(
            width: pinSize,
            height: pinSize,
            point: LatLng(
              double.parse(m['latitude'].toString().trim()),
              double.parse(m['longitude'].toString().trim()),
            ),
            builder: (ctx) => Container(
              child: GestureDetector(
                child: Icon(Icons.location_on, color: NEARLYDARKBLUE),
                onTap: () {
                  var name = m['name'].toString().trim();
                  showPopUp(name, context);
                },
              ),
            ),
          ),
        )
        .toList();
    setState(() {});
    super.initState();
  }

  var pinSize = 30.0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: FlutterMap(
          mapController: _controller,
          options: MapOptions(
            center: new LatLng(48.8566, 2.3522),
            zoom: 11.0,
          ),
          layers: [
            TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c'],
            ),
            MarkerLayerOptions(
              markers: markers,
            ),
          ],
        ),
      ),
    );
  }
}

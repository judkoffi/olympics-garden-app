import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';
import 'package:olympics_garden_app/models/reservation.dart';
import 'package:olympics_garden_app/services/data_service.dart';
import 'package:olympics_garden_app/utils/utils.dart';
import 'package:olympics_garden_app/views/home.dart';

class ShoppingView extends StatefulWidget {
  ShoppingView({
    Key key,
    this.title,
  }) : super(key: key);
  final String title;

  @override
  _ShoppingViewState createState() => _ShoppingViewState();
}

class _ShoppingViewState extends State<ShoppingView> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final DataService database = DataService();
  var titleController = TextEditingController();
  var userKeyController = TextEditingController();

  var paddingBetweenField = Padding(
    padding: EdgeInsets.only(
      top: 15.0,
      left: 10.0,
      right: 10.0,
    ),
  );

  _formBody(textStyle) {
    titleController.text = widget.title;

    return Column(
      children: <Widget>[
        FormBuilder(
          key: _fbKey,
          autovalidate: true,
          initialValue: {},
          readOnly: false,
          child: Column(
            children: <Widget>[
              paddingBetweenField,
              TextField(
                decoration: InputDecoration(
                  labelText: 'Lieu',
                  labelStyle: textStyle,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
                style: textStyle,
                readOnly: true,
                controller: titleController,
              ),
              paddingBetweenField,
              FormBuilderDateTimePicker(
                attribute: 'reservationDate',
                inputType: InputType.date,
                format: DateFormat('dd-MM-yyyy'),
                decoration: InputDecoration(
                  labelText: 'Date de réservation',
                  labelStyle: textStyle,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
                validators: [
                  FormBuilderValidators.required(),
                ],
              ),
              paddingBetweenField,
              FormBuilderDateTimePicker(
                attribute: 'reservationHour',
                inputType: InputType.time,
                format: DateFormat('hh:mm'),
                decoration: InputDecoration(
                  labelText: 'Heure de réservation',
                  labelStyle: textStyle,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
                validators: [
                  FormBuilderValidators.required(),
                ],
              ),
              paddingBetweenField,
              FormBuilderSlider(
                attribute: 'childNumber',
                onChanged: (e) {},
                min: 0.0,
                max: 10.0,
                initialValue: 1.0,
                divisions: 10,
                activeColor: Colors.blueAccent,
                inactiveColor: Colors.pink[100],
                decoration: InputDecoration(
                  labelText: 'Nombre d\'enfants',
                  labelStyle: textStyle,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
                validators: [
                  FormBuilderValidators.required(),
                  FormBuilderValidators.min(1)
                ],
              ),
              paddingBetweenField,
              FormBuilderTextField(
                attribute: 'userKey',
                decoration: InputDecoration(
                  labelText: 'Numéro du ticket',
                  labelStyle: textStyle,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
                controller: userKeyController,
                onChanged: (e) {},
                validators: [
                  FormBuilderValidators.required(),
                  FormBuilderValidators.max(70),
                ],
                keyboardType: TextInputType.text,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            MaterialButton(
              child: Text("Save"),
              padding: const EdgeInsets.all(8.0),
              textColor: Colors.white,
              color: NEARLYBLUE,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              onPressed: () async {
                if (_fbKey.currentState.saveAndValidate()) {
                  var reservationDate =
                      _fbKey.currentState.value['reservationDate'];
                  var reservationHour =
                      _fbKey.currentState.value['reservationHour'];
                  var childNumber = _fbKey.currentState.value['childNumber'];
                  var userKey = _fbKey.currentState.value['userKey'];

                  var elt = Reservation.fromMapObject({
                    'title': widget.title,
                    'hour': reservationHour.toString(),
                    'date': reservationDate.toString(),
                    'userkey': userKey.hashCode.toString(),
                    'childNumber': double.parse(childNumber.toString()).toInt(),
                  });
                  var result = await database.insertReservation(elt);
                  if (result != 0) {
                    var r = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomeView(
                          title: 'Mes Réservation',
                        ),
                      ),
                    );
                  }
                }
              },
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
            ),
            MaterialButton(
              child: Text("Reset"),
              padding: const EdgeInsets.all(8.0),
              textColor: Colors.white,
              color: NEARLYBLUE,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              onPressed: () {
                _fbKey.currentState.reset();
              },
            ),
          ],
        )
      ],
    );
  }

  _scanner() async {
    try {
      var barcode = await BarcodeScanner.scan();
      setState(() {
        userKeyController.text = barcode;
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {}
    } on FormatException catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    var textStyle = Theme.of(context).textTheme.title;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Réserver'),
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: _formBody(textStyle),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          child: IconButton(
            icon: Icon(
              Icons.camera,
            ),
            onPressed: _scanner,
          ),
          onPressed: () {},
          elevation: 2.0,
          backgroundColor: NEARLYBLUE,
        ),
      ),
    );
  }
}

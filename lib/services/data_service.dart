import 'dart:async';

import 'package:olympics_garden_app/models/reservation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DataService {
  static DataService _databaseHelper; // Singleton DatabaseHelper
  static Database _database; // Singleton Database

  var table = 'reservation_table';
  var colId = 'id';
  var colTitle = 'title';
  var colDate = 'date';
  var colHour = 'hour';
  var colChildNumber = 'childNumber';
  var colUserKey = 'userKey';

  DataService._createInstance(); // Named constructor to create instance of DatabaseHelper

  factory DataService() {
    if (_databaseHelper == null) {
      _databaseHelper = DataService._createInstance();
    }
    return _databaseHelper;
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initializeDatabase();
    }
    return _database;
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(
      'CREATE TABLE $table($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colTitle TEXT, '
      '$colHour TEXT, $colDate TEXT, $colChildNumber INTEGER, $colUserKey TEXT)',
    );
  }

  Future<Database> initializeDatabase() async {
    var directory = await getExternalStorageDirectory();
    print(directory.path);
    var path = directory.path + 'reservations.db';

    var reservationDatabase = await openDatabase(
      path,
      version: 1,
      onCreate: _createDb,
    );
    return reservationDatabase;
  }

  Future<List<Map<String, dynamic>>> _getReservationMapList() async {
    var db = await this.database;
    var result = await db.query(table, orderBy: '$colTitle ASC');
    return result;
  }

  Future<List<Reservation>> getReservations() async {
    var mapList = await _getReservationMapList();
    int count = mapList.length;
    print("kdk " + count.toString());
    var reservations = List<Reservation>();
    for (int i = 0; i < count; i++) {
      reservations.add(Reservation.fromMapObject(mapList[i]));
    }
    return reservations;
  }

  // Insert Operation: Insert a todo object to database
  Future<int> insertReservation(Reservation r) async {
    var db = await this.database;
    var result = await db.insert(table, r.toMap());
    return result;
  }

  Future<int> updateReservation(Reservation todo) async {
    var db = await this.database;
    var result = await db.update(
      table,
      todo.toMap(),
      where: '$colId = ?',
      whereArgs: [todo.id],
    );
    return result;
  }

  Future<int> deleteReservation(int id) async {
    var db = await this.database;
    var result = await db.rawDelete(
      'DELETE FROM $table WHERE $colId = $id',
    );
    return result;
  }

  Future<int> getCount() async {
    Database db = await this.database;
    List<Map<String, dynamic>> x = await db.rawQuery(
      'SELECT COUNT (*) from $table',
    );
    return Sqflite.firstIntValue(x);
  }
}

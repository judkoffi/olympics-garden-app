import uuid
import pyqrcode
from pyqrcode import QRCode
import sys


def genarateQrCodeFiles(count):
    for i in range(count):
        # String which represent the QR code
        content = str(uuid.uuid1())
        print(content)
        # Generate QR code
        url = pyqrcode.create(content)
        # Create and save the png file naming "myqr.png"
        url.svg(str(i) + ".png", scale = 16)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: {0} <qrcode number>".format(sys.argv[0]))
        exit(-1)

    genarateQrCodeFiles(int(sys.argv[1]))
